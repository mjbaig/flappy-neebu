package com.mjbaig.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mjbaig.Application;
import com.mjbaig.model.GameConstantsKt;

public class DesktopLauncher {
	public static void main (String[] arg) {

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = GameConstantsKt.getWIDTH();

		config.height = GameConstantsKt.getHEIGHT();

		config.title = GameConstantsKt.getTITLE();

		new LwjglApplication(new Application(), config);

	}
}
