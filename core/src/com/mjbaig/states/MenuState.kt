package com.mjbaig.states

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector3
import com.mjbaig.Application
import com.mjbaig.model.HEIGHT
import com.mjbaig.model.WIDTH
import java.awt.HeadlessException
import java.util.logging.Logger

class MenuState(gameStateManager: GameStateManager): State(gameStateManager) {

    val camera = OrthographicCamera()

    val mouse = Vector3()

    val backgroundImage = Texture("backdrop.png")

    val playImage = Texture("play.png")

    override fun handleInput() {
        if(Gdx.input.justTouched()){
            gameStateManager.set(PlayState(gameStateManager))
        }
    }

    override fun update(deltaTime: Float) {
        handleInput()
    }

    override fun render(spriteBatch: SpriteBatch) {

        spriteBatch.begin()

        spriteBatch.draw(backgroundImage, 0f, 0f, WIDTH.toFloat(), HEIGHT.toFloat())

        spriteBatch.draw(playImage, (WIDTH.toFloat()/2) - (playImage.width/2), (HEIGHT.toFloat()/2) - (playImage.height/2))

        spriteBatch.end()

    }

    override fun dispose() {

        backgroundImage.dispose()

        playImage.dispose()

    }

}