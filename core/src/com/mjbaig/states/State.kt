package com.mjbaig.states

import com.badlogic.gdx.graphics.g2d.SpriteBatch

abstract class State(val gameStateManager: GameStateManager) {

    abstract fun handleInput()

    abstract fun update(deltaTime: Float)

    abstract fun render(spriteBatch: SpriteBatch)

    abstract fun dispose()

}