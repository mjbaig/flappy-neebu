package com.mjbaig.states

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.mjbaig.model.HEIGHT
import com.mjbaig.model.WIDTH
import com.mjbaig.sprites.Bird
import com.mjbaig.sprites.Ground
import com.mjbaig.sprites.Tube

class PlayState(gameStateManager: GameStateManager): State(gameStateManager) {

    private val backgroundTexture = Texture("backdrop.png")

    private val bird = Bird(50f, 300f)

    private val camera = OrthographicCamera()

    private val ground: Ground

    private val tubeSpacing = 125

    private val tubeCount = 4

    private val tubeWidth = 52

    private val tubes = mutableListOf<Tube>()

    init {
        camera.setToOrtho(false, WIDTH.toFloat()/2, HEIGHT.toFloat()/2)

        for( index in tubeCount downTo 0){
            tubes.add(Tube((this.tubeSpacing +index * (this.tubeSpacing + this.tubeWidth)).toFloat()))
        }

        ground = Ground(camera)

    }

    override fun handleInput() {
        if(Gdx.input.justTouched()){
            bird.jump()
        }
    }

    override fun update(deltaTime: Float) {

        val panThreshold = camera.position.x - camera.viewportWidth/2

        handleInput()

        bird.update(deltaTime)

        camera.position.x = bird.position.x + 80f

        this.tubes.forEach{ tube ->
            if((tube.topTubePosition.x + tubeWidth) < panThreshold){
                tube.reposition(tube.topTubePosition.x + (tubeWidth + tubeSpacing)* (tubeCount + 1))
            }

            if(tube.hasColided(bird.birdHitbox)) {
                println("BIRD IS DOWN MAYDAY MAYDAY")
                gameStateManager.set(MenuState(gameStateManager))
            }
        }

        ground.update(camera)

        if(ground.hasColided(bird.birdHitbox)){
            println("BIRD IS DOWN MAYDAY MAYDAY")
            gameStateManager.set(MenuState(gameStateManager))
        }

        camera.update()

    }

    override fun render(spriteBatch: SpriteBatch) {

        spriteBatch.projectionMatrix = camera.combined

        spriteBatch.begin()

        spriteBatch.draw(backgroundTexture, camera.position.x - (camera.viewportWidth / 2), 0f)

        spriteBatch.draw(bird.birdAnimation.getFrame(), bird.position.x, bird.position.y)

        tubes.forEach { tube ->

            spriteBatch.draw(tube.topTubeTexture, tube.topTubePosition.x, tube.topTubePosition.y)

            spriteBatch.draw(tube.bottomTubeTexture, tube.bottomTubePosition.x, tube.bottomTubePosition.y)

        }

        ground.render(spriteBatch)

        spriteBatch.end()
    }

    override fun dispose() {
        backgroundTexture.dispose()
        bird.dispose()
        ground.dispose()
        tubes.map { it.dispose() }
    }

}