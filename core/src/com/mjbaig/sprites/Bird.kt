package com.mjbaig.sprites

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector3
import com.mjbaig.model.HEIGHT

class Bird(xPosition: Float, yPosition: Float) {

    private val gravity = -15f

    private val horizontalVeloticy = 100;

    val position = Vector3(xPosition, yPosition, 0f)

    private val velocity = Vector3(0f,0f,0f)

    val birdSprite = Texture("bird.png")

    val birdHitbox = Rectangle(position.x, position.y, birdSprite.width.toFloat(), birdSprite.height.toFloat())

    private val animationTexture = Texture("birdanimation.png")

    val birdAnimation = Animation(animationTexture, 3, .30f)

    fun update(deltaTime: Float){

        velocity.add(0f, gravity, 0f)

        position.add(horizontalVeloticy * deltaTime, velocity.y * deltaTime, 0f)

        birdHitbox.setPosition(position.x, position.y)

        if(position.y < 0){
            position.y = 0f
        } else if(position.y > HEIGHT) {
            velocity.add(0f, gravity, 0f)
        }

        birdAnimation.update(deltaTime)

    }

    fun jump(){

        velocity.y = 250f

    }

    fun dispose(){
        birdSprite.dispose()
        animationTexture.dispose()
    }

}