package com.mjbaig.sprites

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import java.util.*

class Tube (xPosition: Float) {

    private val tubeLengthFluctuation = 130

    private val tubeGap = 100

    private val lowestTubeOpening = 120

    val topTubeTexture = Texture("toptube.png")

    val bottomTubeTexture = Texture("bottomtube.png")

    val topTubePosition = Vector2()

    val bottomTubePosition = Vector2()

    private lateinit var topTubeHitbox: Rectangle

    private lateinit var bottomTubeHitbox: Rectangle

    private val randomNumberGenerator = Random()

    private val xPosition: Float = xPosition

    init {

        topTubePosition.x = this.xPosition

        topTubePosition.y = (randomNumberGenerator.nextInt(tubeLengthFluctuation) + tubeGap + lowestTubeOpening).toFloat()

        bottomTubePosition.x = this.xPosition

        bottomTubePosition.y = topTubePosition.y - tubeGap - bottomTubeTexture.height

        topTubeHitbox = Rectangle(topTubePosition.x, topTubePosition.y, topTubeTexture.width.toFloat(), topTubeTexture.width.toFloat())

        bottomTubeHitbox = Rectangle(bottomTubePosition.x, bottomTubePosition.y, bottomTubeTexture.width.toFloat(), bottomTubeTexture.height.toFloat())

    }

    fun reposition(newXPosition: Float){

        topTubePosition.x = newXPosition

        topTubePosition.y = (randomNumberGenerator.nextInt(tubeLengthFluctuation) + tubeGap + lowestTubeOpening).toFloat()

        topTubeHitbox.setPosition(topTubePosition.x, topTubePosition.y)

        bottomTubePosition.x = newXPosition

        bottomTubePosition.y = topTubePosition.y - tubeGap - bottomTubeTexture.height

        bottomTubeHitbox.setPosition(bottomTubePosition.x, bottomTubePosition.y)

    }

    fun hasColided(player: Rectangle): Boolean {
        return player.overlaps(topTubeHitbox) || player.overlaps(bottomTubeHitbox)
    }

    fun dispose(){
        topTubeTexture.dispose()
        bottomTubeTexture.dispose()
    }

}