package com.mjbaig.sprites

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion

class Animation(texture: Texture, frameCount: Int, cycleTime: Float) {

    private val frames = mutableListOf<TextureRegion>()

    private val maxFrameTime = cycleTime / frameCount

    private var timeElapsedInCurrentFrame = 0f

    private var currentFrame = 0

    init {
        for(index in (frameCount - 1) downTo 0) {
            frames.add(TextureRegion(texture, index * (texture.width/frameCount), 0, texture.width / frameCount, texture.height))
        }
    }

    fun update(deltaTime: Float){

        timeElapsedInCurrentFrame += deltaTime

        if(timeElapsedInCurrentFrame > maxFrameTime) {
            currentFrame++
            timeElapsedInCurrentFrame = 0f
        }

        if(currentFrame > frames.size - 1){
            currentFrame = 0
        }

    }

    fun getFrame(): TextureRegion {
        return frames[currentFrame]
    }

}