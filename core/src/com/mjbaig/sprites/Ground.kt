package com.mjbaig.sprites

import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector
import com.badlogic.gdx.math.Vector2

class Ground(camera: OrthographicCamera) {

    private val groundHeight = -100f

    private val groundTexture = Texture("ground.png")

    val ground1Position = Vector2(
            camera.position.x - camera.viewportWidth/2,
            groundHeight
    )

    val ground2Position = Vector2(
            camera.position.x - camera.viewportWidth/2 + groundTexture.width,
            groundHeight
    )

    private val groundHitbox = Rectangle(
            0f,
            0f,
            groundTexture.width.toFloat() + ground2Position.x,
            groundTexture.height + groundHeight
    )

    fun render(spriteBatch: SpriteBatch){

        spriteBatch.draw(groundTexture, ground1Position.x, ground1Position.y)

        spriteBatch.draw(groundTexture, ground2Position.x, ground2Position.y)

    }

    fun update(camera: OrthographicCamera){
        if(ground2Position.x <= camera.position.x - camera.viewportWidth/2) {

            ground1Position.x = camera.position.x - camera.viewportWidth/2

            ground2Position.x = camera.position.x - camera.viewportWidth/2 + groundTexture.width

            groundHitbox.setPosition(ground1Position.x, groundHeight)

        }
    }

    fun hasColided(player: Rectangle): Boolean {
        return player.overlaps(groundHitbox)
    }

    fun dispose(){
        groundTexture.dispose()
    }



}